const express = require("express");
const app = express();

//Middleware function
app.use(express.json());

const db = require("./model");
db.mongoose
    .connect(db.url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        dbName: "questionbank_db"
    })
    .then(() => {
        console.log("Connected to the database!");
    })
    .catch(err => {
        console.log("Cannot connect to the database!", err);
        process.exit();
    });


require("./routes/user.router")(app);
require("./routes/question.router")(app);
require("./routes/question-paper.router")(app);
require("./routes/student-question-paper.router")(app);



// simple routes
app.get("/", (req, res) => {
    res.json({message: "Welcome to QuestionBank App..!!"});
});


const PORT = process.env.PORT || 6000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});