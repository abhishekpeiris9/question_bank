const express = require("express");
const db = require("../model")
const bcrypt = require("bcryptjs")
const jwt = require("jsonwebtoken");
const { use } = require("express/lib/router");
const Joi = require('joi');

const User = db.User;

// Create and Save a new User | user registration
exports.createUser = async (req, res) => {
    console.log(req.body)

    //Encrypt user password | salt round (10)
    encryptedPassword = await bcrypt.hash(req.body.password, 10);

    let user = await new User({
        email: req.body.email,
        name: req.body.name,
        streetAddress: req.body.streetAddress,
        mobile: req.body.mobile,
        age: req.body.age,
        gender: req.body.gender,
        country: req.body.country,
        isActive: req.body.isActive,
        password: encryptedPassword

    })

    await user.save(user)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        });
};



// Retrieve all Users from the database.
exports.getUserDetails = (req, res) => {
    User.find({"isActive" : true})
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            res.status(500).send({
                message:
                    err.message || "Some error occurred while retrieving users."
            });
        });
};

// Find a single User with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    console.log(id)
    
    User.findById(id)
        .then(data => {
            User.find({"isActive": false})
            if (!data)
                res.status(404).send({ message: "Not found User with id " + id });
            else res.send(data);
        })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving User with id=" + id });
        });
};


// Update a User by the id in the request
exports.updateUser = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    User.findByIdAndUpdate(id, req.body)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update User with id=${id}. Maybe User was not found!`
                });
            } else res.send({ message: "User was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating User with id=" + id
            });
        });
};

// Delete user details
exports.deleteUser = (req, res) => {
    const id = req.params.id;

    User.findByIdAndUpdate(id, { "isActive": false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete User with id=${id}. Maybe User was not found!`
                });
            } else {
                res.send({
                    message: "User was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete User with id=" + id
            });
        });
};


// User login
exports.login = async (req, res) => {

    if (!req.body) {
        return res.status(400).send({
            message: "empty!"
        });
    }

    const email = req.body.email;
    const password = req.body.password;

    let user = await User.findOne({ "email": email })
        .catch(err => {
            res
                .status(500)
                .send({ message: "Error retrieving User with email : " + email });
        });

    if (user && (await bcrypt.compare(password, user.password))) {

        const token = jwt.sign(
            { user_id: user.id, email },
            "121asa33asa",
            { expiresIn: "2h" }
        )

        console.log("token -- " + token)

        let resData = {
            "name": user.name,
            "email": user.email,
            "token": token,
            "id": user.id
        }

        res.send(resData);
    }
    else
        res.status(400).send("Invalid Credentials..!!")
};


