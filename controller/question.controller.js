const express = require("express");
const db = require("../model")
const Joi = require('joi');

const Question = db.Question;
const QuestionAnswer = db.QuestionAnswer;


// Retrieve all questions with answers from the database.
exports.findAll = async (req, res) => {

    const responseData = [];
    const questions = await getQuestions();

    for (const question in questions) {

        const condition = { questionId: questions[question]._id.toString() };
        console.log(condition)

        const answers = await getAnswers(condition)

        const questionData = {
            "id": questions[question]._id.toString(),
            "category": questions[question].category,
            "name": questions[question].name,
            "questionText": questions[question].questionText,
            "imageUrl": questions[question].imageUrl,
            "isActive": questions[question].isActive,
            "createdAt": questions[question].createdAt,
            "updatedAt": questions[question].updatedAt,
            "answers": answers
        }

        responseData.push(questionData)

    }

    console.log(responseData)
    await res.send(responseData);
};


function getQuestions() {

    return new Promise((resolve, reject) => {
        Question.find({ "isActive": true })
            .then(data => {
                resolve(data)
            })
            .catch(err => {
                res.status(500).send({
                    message:
                        err.message || "Some error occurred while retrieving data."
                });
            });
    })
}

function getAnswers(condition) {

    return new Promise((resolve, reject) => {
        QuestionAnswer.find(condition)
            .then(answers => {
                resolve(answers)
            })
            .catch(err => {
                console.log(err)
            });
    })

}

//Post API for send question with answer
exports.save = async (req, res) => {
    console.log(req.body)
    
    let question = new Question({
        category: req.body.category,
        name: req.body.name,
        questionText: req.body.questionText,
        imageUrl: req.body.imageUrl,
        isActive: req.body.isActive
    })

    const questionSchema = Joi.object({
        category: Joi.number().required(),
        name: Joi.string().required().min(3),
        questionText: Joi.string().required(),
        imageUrl: Joi.string().required(),
        isActive: Joi.boolean(),
        _id: Joi.required()
       
    })

    const validQuestion = questionSchema.validate(question._doc);
    console.log(validQuestion)
    if(validQuestion.error){
        return res.send(validQuestion.error.details[0].message);
    }
    

    let Resdata;

    await question.save(question)
        .then(data => {

            Resdata = data

        })
        .catch(err => {
            console.log(err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the Question."
            });
        });


    let answers = req.body.answers

    for (answer in answers) {
        console.log(answers[answer])

        let question_answer = new QuestionAnswer({
            questionId: Resdata._id.toString(),
            answer: answers[answer].answer,
            isCorrect: answers[answer].isCorrect
        });

        await question_answer.save(question_answer)
            .then(data => {
                console.log("ok")
                answers[answer] = data;
            })
            .catch(err => {
                console.log(err)
            });

    }


    let questionData = {
        "id": Resdata._id.toString(),
        "category": Resdata.category,
        "name": Resdata.name,
        "questionText": Resdata.questionText,
        "imageUrl": Resdata.imageUrl,
        "isActive": Resdata.isActive,
        "createdAt": Resdata.createdAt,
        "updatedAt": Resdata.updatedAt,
        "answers": answers
    }

    console.log(questionData)
    res.send(questionData);
};


//Find a single User with an id
exports.findOne = async (req, res) => {

    const id = req.params.id;
    console.log(id)

    const question = await Question.findById(id)
        .catch(err => {
            console.log(err)
            res.status(500)
                .send({ message: "Error retrieving Question with id=" + id });
        });

    console.log(question)

    const answers = await QuestionAnswer.find({ "questionId": id })
    console.log(answers)

    const questionData = {
        "id": question._id.toString(),
        "category": question.category,
        "name": question.name,
        "questionText": question.questionText,
        "imageUrl": question.imageUrl,
        "isActive": question.isActive,
        "createdAt": question.createdAt,
        "updatedAt": question.updatedAt,
        "answers": answers
    }

    await res.send(questionData);

};

//Update API for update question details
exports.update = async (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    const question = await Question.findByIdAndUpdate(id, req.body, {
        new : true
        })
        .catch(err => {
            console.log(err)
            res.status(500).send({
                message: "Error updating Question with id=" + id
            });
        });

        
    let answers = req.body.answers;
    let answerList = []

    for (answer in answers) {
        console.log(answers[answer]);

        let question_answer = {
            "answer": answers[answer].answer,
            "isCorrect": answers[answer].isCorrect
        }

        console.log(question_answer)


        let savedAnswer = await QuestionAnswer.findByIdAndUpdate(answers[answer].id, question_answer, {
            new: true
        })
        .catch(err => {
            console.log(err)
        })
    
        console.log(savedAnswer)
        answerList.push(savedAnswer)

    }
    const questionData = {
        "id": question._id.toString(),
        "category": question.category,
        "name": question.name,
        "questionText": question.questionText,
        "imageUrl": question.imageUrl,
        "isActive": question.isActive,
        "createdAt": question.createdAt,
        "updatedAt": question.updatedAt,
        "answers": answerList

    }

   await res.send(questionData);
};

// Delete API for questions
exports.delete = (req, res) => {
    const id = req.params.id;

    Question.findByIdAndUpdate(id, { "isActive": false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Question with id=${id}. Maybe Question was not found!`
                });
            } else {
                res.send({
                    message: "Question was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete Question with id=" + id
            });
        });
};