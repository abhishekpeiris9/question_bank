const express = require("express");
const db = require("../model")

const Question = db.Question;
const QuestionPaper = db.QuestionPaper;
const QuestionAnswer = db.QuestionAnswer;

// Create Question Paper 
exports.save = async (req, res) => {
    console.log(req.body)

    let questionPaper = await new QuestionPaper({
        duration: req.body.duration,
        isActive: req.body.isActive,
        questions: req.body.questions,
        questionCount: req.body.questionCount
    })

    await questionPaper.save(questionPaper)
        .then(data => {
            res.send(data);
        })
        .catch(err => {
            console.log(err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        });

};


// Get Question Paper with the random count
exports.getPapers = async (req, res) => {
    console.log(req.body)

    const papers = await QuestionPaper.find({ "isActive": true })
        .catch(err => {
            console.log(err)
        });

    let resData = [];

    for (paper in papers) {

        let questions = [];
       // let questionIds = [];
        for (let i = 0; i < papers[paper].questionCount; i++) {
            console.log(papers[paper].questions)
            let questionId = await getRandomQuestionId(papers[paper].questions);

            let question = await Question.findById(questionId)
                .catch(err => {
                    res.status(500).send({
                        message:
                            err.message || "Some error occurred while retrieving data."
                    });
                });

            console.log(questionId + " -- " + question)
            

            let answers = await QuestionAnswer.find({ "questionId": questionId })
                .catch(err => {
                    console.log(err)
                });

            console.log("answers --- " + answers)

            const questionData = {
                "id": question._id.toString(),
                "category": question.category,
                "name": question.name,
                "questionText": question.questionText,
                "imageUrl": question.imageUrl,
                "isActive": question.isActive,
                "createdAt": question.createdAt,
                "updatedAt": question.updatedAt,
                "answers": answers
            }

            questions.push(questionData)
           // questionIds.push(questionId)
        }

        let paperData = {
            "id": papers[paper]._id.toString(),
            "duration": papers[paper].duration,
            "questionCount": papers[paper].questionCount,
            "questions": questions,
            "createdAt": papers[paper].createdAt,
            "updatedAt": papers[paper].updatedAt,
        }

        resData.push(paperData)
    }

    res.send(resData)

};


function getRandomQuestionId(questions) {
    return questions[Math.floor(Math.random() * questions.length)]
    
}


// Update a Question Paper by the id in the request
exports.updatePaper = (req, res) => {
    if (!req.body) {
        return res.status(400).send({
            message: "Data to update can not be empty!"
        });
    }

    const id = req.params.id;

    QuestionPaper.findByIdAndUpdate(id, req.body)
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot update Question Paper with id=${id}. Maybe Question Paper was not found!`
                });
            } else res.send({ message: "Question Paper was updated successfully." });
        })
        .catch(err => {
            res.status(500).send({
                message: "Error updating Question Paper with id=" + id
            });
        });
};

// Delete a Question Paper with the specified id in the request
exports.deletePaper = (req, res) => {
    const id = req.params.id;

    QuestionPaper.findByIdAndUpdate(id, { "isActive": false })
        .then(data => {
            if (!data) {
                res.status(404).send({
                    message: `Cannot delete Question Paper with id=${id}. Maybe Question Paper was not found!`
                });
            } else {
                res.send({
                    message: "Question Paper was deleted successfully!"
                });
            }
        })
        .catch(err => {
            res.status(500).send({
                message: "Could not delete User with id=" + id
            });
        });
};
