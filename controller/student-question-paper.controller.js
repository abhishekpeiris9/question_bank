const express = require("express");
const db = require("../model")

const QuestionAnswer = db.QuestionAnswer;
const StudentQuestionPaper = db.StudentQuesPaper;
const MarkedAnswer = db.MarkedAnswer;

// Create student question paper
exports.save = async (req, res) => {
    console.log(req.body)

    const questions = req.body.questions;
    const questionIds = []

    for (question in questions) {
        questionIds.push(questions[question].question_id)
    }

    console.log("questionIds -- " + questionIds)

    let studentQuestionPaper = await new StudentQuestionPaper({
        paperId: req.body.paperId,
        questions: questionIds,
        attempt: req.body.attempt,
        duration: req.body.duration,
        userId: req.body.userId,
        lastCompleted: new Date(),
        isActive: true
    })

    studentQuestionPaper = await studentQuestionPaper.save(studentQuestionPaper)
        .catch(err => {
            console.log(err)
            res.status(500).send({
                message:
                    err.message || "Some error occurred while creating the User."
            });
        });

        
    const markedAnswers = [];
    let correctAnswerCount = 0;

    for (question in questions) {

        let correctAnswer = await QuestionAnswer.findOne({ "questionId": questions[question].question_id, "isCorrect": true })
            .catch(err => {
                console.log(err)
            });

        console.log("user answer -- " + questions[question].answer_id + " correct answer -- " + correctAnswer._id.toString())

        answerCorrectionStatus = questions[question].answer_id == correctAnswer._id.toString();
        if (answerCorrectionStatus) {
            correctAnswerCount++
        }

        let markedAnswer = await new MarkedAnswer({
            questionId: questions[question].question_id,
            answer: questions[question].answer_id,
            isCorrect: answerCorrectionStatus,
            studentQuestionPaperId: studentQuestionPaper._id.toString()
        })

        markedAnswer = await markedAnswer.save(markedAnswer)
            .catch(err => {
                console.log(err)
            });

        markedAnswers.push(markedAnswer);

    }

    const resData = {
        "id": studentQuestionPaper._id.toString(),
        "userId": studentQuestionPaper.userId,
        "paperId": studentQuestionPaper.paperId,
        "questions": questionIds,
        "attempt": studentQuestionPaper.attempt,
        "duration": studentQuestionPaper.duration,
        "lastCompleted": studentQuestionPaper.lastCompleted,
        "createdAt": studentQuestionPaper.createdAt,
        "updatedAt": studentQuestionPaper.updatedAt,
        "markedAnswers" : markedAnswers,
        "correctAnswerCount" : correctAnswerCount

    }

    res.send(resData)

};



