module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            id:Number,
            questionId:String,
            answer: String,
            isCorrect:Boolean,
            studentQuestionPaperId : String
            

        },
        {timestamps: true}
    );

    schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const MarkedAnswer = mongoose.model("marked_answer", schema);
    return MarkedAnswer;
};




 
