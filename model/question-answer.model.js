module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            id:Number,
            questionId:String,
            answer:String,
            isCorrect:Boolean

        },
        {timestamps: true}
    );

    schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const QuestionAnswer = mongoose.model("question_answer", schema);
    return QuestionAnswer;
};




 
