module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            id:Number,
            category:Number,
            name:String,
            questionText:String,
            imageUrl:String,
            isActive:Boolean

        },
        {timestamps: true}
    );

    schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const Question = mongoose.model("question", schema);
    return Question;
};



