module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            id:Number,
            questions: [],
            duration:Number,
            isActive:Boolean,
            questionCount: Number

        },
        {timestamps: true}
    );

    schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const Question = mongoose.model("question-paper", schema);
    return Question;
};



