const dbConfig = require("../config/db.config.js");

const mongoose = require("mongoose");
mongoose.Promise = global.Promise;

const db = {};
db.mongoose = mongoose;
db.url = dbConfig.url;

db.Question = require("./question.model.js")(mongoose);
db.QuestionAnswer = require("./question-answer.model.js")(mongoose);
db.User = require("./user.model.js")(mongoose);
db.QuestionPaper = require("./question-paper.model.js")(mongoose);
db.StudentQuesPaper = require("./student-question-paper.model.js")(mongoose);
db.MarkedAnswer = require("./marked-answer.model.js")(mongoose);


module.exports = db;
