module.exports = mongoose => {
    const schema = mongoose.Schema(
        {
            id:Number,
            paperId:String,
            questions:[],
            attempt:Number,
            duration: Number,
            lastCompleted: Date,
            isActive: Boolean,
            userId: String

        },
        {timestamps: true}
    );

    schema.method("toJSON", function() {
        const { __v, _id, ...object } = this.toObject();
        object.id = _id;
        return object;
    });

    const StudentQuesPaper = mongoose.model("student_question_paper", schema);
    return StudentQuesPaper;
};




 
