const {use} = require("express/lib/router");
const questionpaper = require("../controller/question-paper.controller");

module.exports = app => {
  
    const router = require("express").Router();

    // Create a new post for send question with answer
    router.post("/", questionpaper.save);

    //Retrieve all question with answers details
    router.get("/", questionpaper.getPapers);

    router.put("/:id", questionpaper.updatePaper);

    router.delete("/:id", questionpaper.deletePaper);


    app.use("/api/questionpaper", router);
};
