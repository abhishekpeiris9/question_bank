const {use} = require("express/lib/router");
const question = require("../controller/question.controller");
module.exports = app => {
  
    const router = require("express").Router();

    // Create a new post for send question with answer
    router.post("/", question.save);

    // Retrieve all question with answers details
    router.get("/", question.findAll);

    // Retrieve a single user with id
    router.get("/:id", question.findOne);

    router.put("/:id", question.update);

    router.delete("/:id", question.delete);

    app.use("/api/question", router);
};
