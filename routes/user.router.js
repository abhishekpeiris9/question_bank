const {use} = require("express/lib/router");
const user = require("../controller/user.controller");
const auth = require("../middleware/auth");

module.exports = app => {
  
    const router = require("express").Router();

    // Create a new post for user
    router.post("/", user.createUser);

    router.post("/login", user.login);

    // Retrieve all user details
    router.get("/", user.getUserDetails);

    // Retrieve a single user with id
    router.get("/:id", user.findOne);
    
     // Update a user with id
    router.put("/:id", auth, user.updateUser);
    
    //Delete a user with id
    router.delete("/:id", user.deleteUser);

    app.use("/api/user", router);
};
