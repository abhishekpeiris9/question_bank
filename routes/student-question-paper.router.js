const {use} = require("express/lib/router");
const studentQuestionPaper = require("../controller/student-question-paper.controller");

module.exports = app => {
  
    const router = require("express").Router();

    // Create a new post for send student question paper
    router.post("/", studentQuestionPaper.save);

    app.use("/api/student-question-paper", router);
};
